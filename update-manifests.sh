#!/bin/sh -x

SRCROOT="$( CDPATH='' cd -- "$(dirname "$0")" && pwd -P )"
AUTOGENMSG="# This is an auto-generated file. DO NOT EDIT"

echo "${AUTOGENMSG}" > "${SRCROOT}/install_htpc.yaml"
kustomize build "${SRCROOT}/base" >> "${SRCROOT}/install_htpc.yaml"
